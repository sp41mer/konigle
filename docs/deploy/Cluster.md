# Cluster deployment
Deployment to cluster via Kubernetes

## Set-up master node
```sudo kubeadm init --pod-network-cidr=10.244.0.0/16```
```kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml```

## Add node to cluster
1. Create token at master node:
```kubeadm token create --print-join-command```

2. Join node with command:
Example: ```kubeadm join <MASTER IP>:6443 --token <TOKEN> --discovery-token-ca-cert-hash <HASH>```

## Update image version
1. via console editor:
```KUBE_EDITOR="nano" kubectl edit deployment django-deployment```
2. via Dashboard: connect to `<DASHBOARD-IP>`, then just change image parameter

## Remove node from cluster
1. At worker node:
```kubeadm reset```
2. At master node:
```kubectl delete node <NODE_NAME>}```