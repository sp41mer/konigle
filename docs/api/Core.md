## /api/core/deviations/

Endpoint to get standard deviations

GET:

Response:
```
[
    {
        "casual_deviation": float,
        "registered_deviation": float,
        "day": int
    }
]
```

## /api/core/cache/
Endpoint to get standard deviations from cache

GET:

Response
```
[
            {
                "registered_day_standard_deviation": float,
                "casual_day_standard_deviation": float,
                "day": int
            }
]
```

## /api/core/hourly-sharing/
Endpoint to get data about sharing

GET: 

Response
```[
    {
        "id": int,
        "dteday": str,
        "season": int,
        "year": int,
        "month": int,
        "hour": int,
        "is_holiday": boolean,
        "weekday": int,
        "is_workingday": boolean,
        "weathersit": int,
        "temp": float,
        "atemp": float,
        "hum": float,
        "windspeed": float,
        "casual": int,
        "registered": int,
        "count": int
    }
]
```

POST:

Request 
```
{
    "dteday": str,
    "season": int,
    "year": int,
    "month": int,
    "hour": int,
    "is_holiday": boolean,
    "weekday": int,
    "is_workingday": boolean,
    "weathersit": int,
    "temp": float,
    "atemp": float,
    "hum": float,
    "windspeed": float,
    "casual": int,
    "registered": int,
    "count": int
}
```

Response
```
{
    "dteday": str,
    "season": int,
    "year": int,
    "month": int,
    "hour": int,
    "is_holiday": boolean,
    "weekday": int,
    "is_workingday": boolean,
    "weathersit": int,
    "temp": float,
    "atemp": float,
    "hum": float,
    "windspeed": float,
    "casual": int,
    "registered": int,
    "count": int
}
```

