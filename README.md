# Test assigment

## How to run fast
`docker-compose up`

## How to enable post save caching
There is `CACHE_FOR_MATH_ENABLED` option in settings. If you want to recalculate deviations after each new HourlySharin instance, you should enable it

## How to populate database with data

`./manage.py populate_from_file <hour.csv file location>`

## How to set up local environment

1) Install requirements
`pip install -r requirements.txt`

2) Create database
`python manage.py migrate`

3) Populate db with data from csv file [optional]
 `./manage.py populate_from_file <hour.csv file location>`

4) Prepare cache [optional]
`python manage.py recreate_cache`

5) Create superuser
`python manage.py createsuperuser`

6) Run server
`python manage.py runserver`


## Deployment guides:
1. Local deployment: `docs/deploy/Local.md`
2. Cluster guide: `docs/deploy/Cluster.md`


## Remote server
77.244.213.141:8080

