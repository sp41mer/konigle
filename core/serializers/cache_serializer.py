from rest_framework.serializers import ModelSerializer

from core.models import StandardDeviationCache


class CacheSerializer(ModelSerializer):

    class Meta:
        model = StandardDeviationCache
        fields = ('registered_day_standard_deviation', 'casual_day_standard_deviation', 'day')
