from rest_framework.serializers import ModelSerializer

from core.models import HourlySharing


class HourlySharingSerializer(ModelSerializer):

    class Meta:
        model = HourlySharing
        fields = '__all__'
