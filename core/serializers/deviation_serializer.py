from collections import OrderedDict

from rest_framework.serializers import Serializer, FloatField, IntegerField


class DeviationSerializer(Serializer):

    casual_deviation = FloatField()
    registered_deviation = FloatField()
    day = IntegerField()


