from django.core.management import BaseCommand

from core.models import StandardDeviationCache, HourlySharing


class Command(BaseCommand):

    def handle(self, *args, **options):
        deviations = HourlySharing.objects.calculate_standard_deviation()
        for value in deviations:
            cache_object, _ = StandardDeviationCache.objects.get_or_create(day=value['day'])
            cache_object.registered_day_standard_deviation = value['registered_deviation']
            cache_object.casual_day_standard_deviation = value['casual_deviation']
            cache_object.registered_day_average = value['registered_average']
            cache_object.casual_day_average = value['casual_average']
            cache_object.count = value['counter']
            cache_object.save()

