import csv

from django.core.management import BaseCommand

from core.models import HourlySharing


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('filepath', type=str)

    def read_file(self):
        with open(self.filepath) as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for line in reader:
                HourlySharing.objects.create(
                    dteday=line[1],
                    season=float(line[2]),
                    year=float(line[3]),
                    month=float(line[4]),
                    hour=float(line[5]),
                    is_holiday=float(line[6]),
                    weekday=float(line[7]),
                    is_workingday=float(line[8]),
                    weathersit=float(line[9]),
                    temp=float(line[10]),
                    atemp=float(line[11]),
                    hum=float(line[12]),
                    windspeed=float(line[13]),
                    casual=float(line[14]),
                    registered=float(line[15]),
                    count=float(line[16]),
                )

    def handle(self, *args, **options):
        self.filepath = options['filepath']
        self.read_file()
