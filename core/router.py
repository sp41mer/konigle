from rest_framework.routers import DefaultRouter
from core import views

router = DefaultRouter(trailing_slash=True)

router.register('deviations', views.DeviationsViewSet, 'deviations')
router.register('cache', views.CacheViewSet, 'cache')
router.register('hourly-sharing', views.HourlySharingViewSet, 'hourly-sharing')
