from rest_framework.test import APIClient, APITestCase
from django_dynamic_fixture import G

from core.models import HourlySharing, StandardDeviationCache


class BaseApiTest(APITestCase):

    def setUp(self):
        self.client: APIClient = APIClient()
        self.hourly_sharing: HourlySharing = G(HourlySharing)
        self.cache: StandardDeviationCache = G(StandardDeviationCache, day=0)

