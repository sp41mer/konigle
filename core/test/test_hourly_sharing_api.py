import json

from django.urls import reverse
from rest_framework import status
from rest_framework.response import Response

from . import BaseApiTest


class HourlySharingApiTest(BaseApiTest):

    def test_list(self):
        api_json = json.loads(json.dumps([
            {
                'id': self.hourly_sharing.id,
                'dteday': str(self.hourly_sharing.dteday),
                'season': self.hourly_sharing.season,
                'year': self.hourly_sharing.year,
                'month': self.hourly_sharing.month,
                'hour': self.hourly_sharing.hour,
                'is_holiday': self.hourly_sharing.is_holiday,
                'weekday': self.hourly_sharing.weekday,
                'is_workingday': self.hourly_sharing.is_workingday,
                'weathersit': self.hourly_sharing.weathersit,
                'temp': self.hourly_sharing.temp,
                'atemp': self.hourly_sharing.atemp,
                'hum': self.hourly_sharing.hum,
                'windspeed': self.hourly_sharing.windspeed,
                'casual': self.hourly_sharing.casual,
                'registered': self.hourly_sharing.registered,
                'count': self.hourly_sharing.count
            }
        ]))
        response: Response = self.client.get(reverse('hourly-sharing-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), api_json)
