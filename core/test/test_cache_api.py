import json

from django.urls import reverse
from rest_framework import status
from rest_framework.response import Response

from . import BaseApiTest


class CacheApiTest(BaseApiTest):

    def test_list(self):
        api_json = json.loads(json.dumps([
            {
                "registered_day_standard_deviation": self.cache.registered_day_standard_deviation,
                "casual_day_standard_deviation": self.cache.casual_day_standard_deviation,
                "day": self.cache.day
            }
        ]))
        response: Response = self.client.get(reverse('cache-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), api_json)
