from django.contrib import admin
from core import models

admin.site.register(models.StandardDeviationCache)
admin.site.register(models.HourlySharing)
