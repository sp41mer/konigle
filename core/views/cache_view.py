from rest_framework.viewsets import ReadOnlyModelViewSet

from core.models import StandardDeviationCache
from core.serializers import CacheSerializer


class CacheViewSet(ReadOnlyModelViewSet):
    queryset = StandardDeviationCache.objects.all()
    serializer_class = CacheSerializer
