from rest_framework.viewsets import ReadOnlyModelViewSet

from core.models import HourlySharing
from core.serializers import DeviationSerializer


class DeviationsViewSet(ReadOnlyModelViewSet):

    serializer_class = DeviationSerializer

    def get_queryset(self):
        return HourlySharing.objects.calculate_standard_deviation()
