from rest_framework.viewsets import ReadOnlyModelViewSet

from core.models import HourlySharing
from core.serializers import HourlySharingSerializer


class HourlySharingViewSet(ReadOnlyModelViewSet):
    queryset = HourlySharing.objects.all()
    serializer_class = HourlySharingSerializer
