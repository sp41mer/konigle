from typing import TYPE_CHECKING
import multiprocessing
from django.db import models

from .hourly_sharing import HourlySharing

NUM_PROC = 4


class StandardDeviationCache(models.Model):

    day = models.IntegerField()
    casual_day_standard_deviation = models.FloatField(default=0)
    casual_day_average = models.FloatField(default=0)
    registered_day_standard_deviation = models.FloatField(default=0)
    registered_day_average = models.FloatField(default=0)
    count = models.IntegerField(default=0)

    @staticmethod
    def count_deviation(values, average, procnum, return_dict):
        print('Hello from Proccess {}'.format(procnum))
        casual_deviation = 0
        registered_deviation = 0
        for x in values:
            casual_deviation += abs(x[0] - average[0])**2
            registered_deviation += abs(x[1] - average[1])**2
        return_dict[procnum] = (casual_deviation, registered_deviation)

    def _get_historical_data(self, instance: 'HourlySharing'):
        self.historical_data = HourlySharing.objects.filter(weekday=instance.weekday).\
            exclude(pk=instance.pk).values_list('casual', 'registered')
        self.length = len(self.historical_data)
        self.chunk_size = int(self.length / NUM_PROC) + 1

    def update_weekday_cache(self, instance: 'HourlySharing'):
        self._get_historical_data(instance)
        chunks = self._get_chunks()

        self.count += 1

        self.casual_day_average = (((self.count - 1) * self.casual_day_average)
                                   + instance.casual) / self.count
        self.registered_day_average = (((self.count - 1) * self.registered_day_average)
                                       + instance.registered) / self.count

        casual_deviation, registered_deviation = self._update_weekday_cache(chunks)

        current_casual_deviation = abs(instance.casual - self.casual_day_average)**2
        self.casual_day_standard_deviation = ((casual_deviation + current_casual_deviation) / self.count)**(1/2)

        current_registered_deviation = abs(instance.registered - self.registered_day_average)**2
        self.registered_day_standard_deviation = ((registered_deviation + current_registered_deviation) / self.count)**(1/2)

        self.save()

    def _update_weekday_cache(self, chunks):
        sum_of_casual_deviations = 0
        sum_of_registered_deviations = 0
        manager = multiprocessing.Manager()
        return_dict = manager.dict()
        jobs = []
        for i in range(len(chunks)):
            p = multiprocessing.Process(target=self.count_deviation, args=(
                chunks[i], (self.casual_day_average, self.registered_day_average), i, return_dict
            ))
            jobs.append(p)
            p.start()
        for proc in jobs:
            proc.join()
        for value in return_dict.values():
            sum_of_casual_deviations += value[0]
            sum_of_registered_deviations += value[1]
        return sum_of_casual_deviations, sum_of_registered_deviations

    def _get_chunks(self):
        return [self.historical_data[x:x+self.chunk_size] for x in range(0, len(self.historical_data), self.chunk_size)]
