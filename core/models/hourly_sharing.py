import multiprocessing

from django.db import models, ProgrammingError, OperationalError
from django.db.models import Avg
from django.db.models.signals import post_save
from django.dispatch import receiver

from conf.settings import CACHE_FOR_MATH_ENABLED


class HourlySharingManager(models.Manager):

    @staticmethod
    def count_deviation_by_days(average_query, values, counter, procnum, return_dict):
        casual_average = float(average_query['casual__avg'])
        registered_average = float(average_query['registered__avg'])
        casual_deviation = 0
        registered_deviation = 0
        for x in values:
            casual_deviation += abs(float(x[0]) - casual_average) ** 2
            registered_deviation += abs(float(x[1]) - registered_average) ** 2
        return_dict[procnum] = {'casual_deviation': (casual_deviation / counter)**(1/2),
                                'registered_deviation': (registered_deviation / counter)**(1/2),
                                'casual_average': casual_average,
                                'registered_average': registered_average,
                                'counter': counter,
                                'day': procnum}

    def calculate_standard_deviation(self):
        manager = multiprocessing.Manager()
        return_dict = manager.dict()
        jobs = []
        for i in range(7):
            average_query = HourlySharing.objects.filter(weekday=i).aggregate(Avg('casual'), Avg('registered'))
            values = HourlySharing.objects.filter(weekday=i).values_list('casual', 'registered')
            p = multiprocessing.Process(target=self.count_deviation_by_days, args=(average_query, values,
                                                                                   len(values), i, return_dict))
            jobs.append(p)
            p.start()
        for proc in jobs:
            proc.join()
        return return_dict.values()


class HourlySharing(models.Model):
    dteday = models.DateField()
    season = models.IntegerField()
    year = models.IntegerField()
    month = models.IntegerField()
    hour = models.IntegerField()
    is_holiday = models.BooleanField()
    weekday = models.IntegerField()
    is_workingday = models.BooleanField()
    weathersit = models.IntegerField()
    temp = models.FloatField()
    atemp = models.FloatField()
    hum = models.FloatField()
    windspeed = models.FloatField()
    casual = models.IntegerField()
    registered = models.IntegerField()
    count = models.IntegerField()

    objects = HourlySharingManager()


@receiver(post_save, sender=HourlySharing, dispatch_uid="update_stock_count")
def update_cache(sender, instance: HourlySharing, created, **kwargs):
    if created and CACHE_FOR_MATH_ENABLED:
        from .standard_deviation_cache import StandardDeviationCache
        day_cache, _ = StandardDeviationCache.objects.get_or_create(day=instance.weekday)
        day_cache.update_weekday_cache(instance)
